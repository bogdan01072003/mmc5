import numpy
import matplotlib.pyplot as plt

s = "P(x) = "
xo = []
yo = []
x2 = -3
n1 = 200
h = 6 / n1
for i in range(n1):
    if x2 <= 0 and x2 >= -3:
        xo.append(x2)
        yo.append(pow((x2 + 1), 2) + 1)
        x2 = x2 + h
    elif x2 > 0 and x2 <= 1:
        xo.append(x2)
        yo.append(1 - 8 * pow((x2 - 0.5), 3))
        x2 = x2 + h
    elif x2 > 1 and x2 <= 3:
        xo.append(x2)
        yo.append(x2 - 1)
        x2 = x2 + h
def differences(xs, ys, k):
    result = 0
    for j in range(k + 1):
        mul = 1
        for i in range(k + 1):
            if i != j:
                mul *= xs[j] - xs[i]
        result += ys[j]/mul
    return result

def prints(xs,ys,name,z):
    plt.scatter(xs, ys)
    plt.plot(xs, ys)
    plt.xlim(-5, 5)
    plt.suptitle(name)
    plt.ylim(0, z)
    plt.show()

    xo = []
    yo = []
    x1 = -3
    n = 200
    h = 6 / n
    for i in range(n):
        if x1 <= 0 and x1 >= -3:
            xo.append(x1)
            yo.append(pow((x1 + 1), 2) + 1)
            x1 = x1 + h
        elif x1 > 0 and x1 <= 1:
            xo.append(x1)
            yo.append(1 - 8 * pow((x1 - 0.5), 3))
            x1 = x1 + h
        elif x1 > 1 and x1 <= 3:
            xo.append(x1)
            yo.append(x1 - 1)
            x1 = x1 + h

prints(xo, yo, "Function", 8)

def create_Newton_polynomial(xs, ys):
    div_diff = []
    for i in range(1, len(xs)):
        div_diff.append(differences(xs, ys, i))
    def newton_polynomial(x):
        global s
        s += str(ys[0])
        s += " "
        result = ys[0]
        for k in range(1, len(ys)):
            mul = 1
            s += "+("
            for j in range(k):
                s = s + "(x-(" + str(xs[j]) + "))"
                mul *= (x-xs[j])
                s = s + "*" + "(" + str(div_diff[k - 1]) + ")"
            result += div_diff[k-1]*mul
            s += ")"
        return result

    return newton_polynomial

def rivno1(n):
    xs = []
    ys = []
    x1 = -3
    h = 6/n
    for i in range(n):
        if x1<=0 and x1>=-3:
            xs.append(x1)
            ys.append(pow((x1+1), 2) + 1)
            x1=x1+h
        elif x1>0 and x1<=1:
            xs.append(x1)
            ys.append(1-8*pow((x1 -0.5), 3))
            x1 = x1 + h
        elif x1>1 and x1<=3:
            xs.append(x1)
            ys.append(x1-1)
            x1 = x1 + h
    print("VALUES_OF_FUNCTION\n")
    for i in range(n):
        print("x = {:.4f}\t y = {:.4f}".format(xs[i],ys[i]))
    new_pol1 = create_Newton_polynomial(xs, ys)
    print("VALUES_OF_POLINOM\n")
    yy=[]
    for i in range(n):
        yy.append(new_pol1(xs[i]))
        print("x = {:.4f}\t y = {:.4f}".format(xs[i], yy[i]))
    print("\nPOLYNOM\n")
    print(s)
    prints(xs,yy,"Norm Rospodil",8)

def chebyshev(a, b, n):
    i = numpy.array(range(n))
    x = numpy.cos((2*i+1)*numpy.pi/(2*(n)))
    return 0.5*(b-a)*x+0.5*(b+a)

def rivno2(a,b, n):
    xs = list(chebyshev(a, b, n))
    xs.reverse()
    ys = []
    for x1 in xs:
        if x1<=0 and x1>=-3:
            ys.append(pow((x1+1), 2) + 1)
        elif x1>0 and x1<=1:
            ys.append(1-8*pow((x1 -0.5), 3))
        elif x1>1 and x1<=3:
            ys.append(x1-1)
    print("VALUES_OF_FUNCTION\n")
    for i in range(n):
        print("{:.4f} {:.4f}".format(xs[i], ys[i]))
    new_pol1 = create_Newton_polynomial(xs, ys)
    print("VALUES_OF_POLINOM\n")
    yy = []
    for i in range(n):
        yy.append(new_pol1(xs[i]))
        print("x = {:.4f}\t y = {:.4f}".format(xs[i], yy[i]))
    print("\nPOLYNOM\n")
    print(s)
    prints(xs, yy, "Chebyshev Rospodil",8)


print('\n\nRivnoviddal\n\n')
rivno1(11)
s = "P(x) = "
print('\n\nCHEBYSHEV\n\n')
rivno2(-3,3,11)



class SplineTuple:
    def __init__(self, a, b, c, d, x):
        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.x = x



def BuildSpline(x, y, n):
    splines = [SplineTuple(0, 0, 0, 0, 0) for _ in range(0, n)]
    for i in range(0, n):
        splines[i].x = x[i]
        splines[i].a = y[i]

    splines[0].c = splines[n - 1].c = 0.0
    alpha = [0.0 for _ in range(0, n - 1)]
    beta = [0.0 for _ in range(0, n - 1)]

    for i in range(1, n - 1):
        hi = x[i] - x[i - 1]
        hi1 = x[i + 1] - x[i]
        A = hi
        C = 2.0 * (hi + hi1)
        B = hi1
        F = 6.0 * ((y[i + 1] - y[i]) / hi1 - (y[i] - y[i - 1]) / hi)
        z = (A * alpha[i - 1] + C)
        alpha[i] = -B / z
        beta[i] = (F - A * beta[i - 1]) / z

    for i in range(n - 2, 0, -1):
        splines[i].c = alpha[i] * splines[i + 1].c + beta[i]
    for i in range(n - 1, 0, -1):
        hi = x[i] - x[i - 1]
        splines[i].d = (splines[i].c - splines[i - 1].c) / hi
        splines[i].b = hi * (2.0 * splines[i].c + splines[i - 1].c) / 6.0 + (y[i] - y[i - 1]) / hi
    return splines



def Interpolate(splines, x):
    if not splines:
        return None

    n = len(splines)
    s = SplineTuple(0, 0, 0, 0, 0)

    if x <= splines[0].x:
        s = splines[0]
    elif x >= splines[n - 1].x:
        s = splines[n - 1]
    else:
        i = 0
        j = n - 1
        while i + 1 < j:
            k = i + (j - i) // 2
            if x <= splines[k].x:
                j = k
            else:
                i = k
        s = splines[j]

    dx = x - s.x
    return s.a + (s.b + (s.c / 2.0 + s.d * dx / 6.0) * dx) * dx;


x = []
y = []
n = 11
x1 = -3
h = 6 / n
for i in range(n):
    if x1 <= 0 and x1 >= -3:
        x.append(x1)
        y.append(pow((x1 + 1), 2) + 1)
        x1 = x1 + h
    elif x1 > 0 and x1 <= 1:
            x.append(x1)
            y.append(1 - 8 * pow((x1 - 0.5), 3))
            x1 = x1 + h
    elif x1 > 1 and x1 <= 3:
            x.append(x1)
            y.append(x1 - 1)
            x1 = x1 + h

spline = BuildSpline(x, y, len(x))

plt.scatter(x, y)
plt.plot(x, y)
plt.xlim (-5, 5)
plt.suptitle("Spline")
plt.ylim (0,8)
plt.scatter(n, Interpolate(spline, n))
plt.show()

spline2 = BuildSpline(x, y, len(x))
print("\n\nSpline\n\n")
for z in spline2:
    print("a = {:.4f}\t b = {:.4f}\t c = {:.4f}\t d = {:.4f}\t start = {:.4f}".format(z.a,z.b,z.c,z.d,z.x))
